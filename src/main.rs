use argmin::prelude::*;
use argmin::solver::neldermead::NelderMead;
use colored_truecolor::Colorize;
use prisma::color_space::named::SRgb;
use prisma::color_space::{ConvertFromXyz, ConvertToXyz};
use prisma::encoding::EncodableColor;
use prisma::lms::{CieCam2002, Lms};
use prisma::{FromColor, FromTuple, Rgb, Xyz};

type WhitePoint = prisma::white_point::deg_2::D65;
type Lab = prisma::Lab<f64, WhitePoint>;

fn srgb2lab(srgb: &Rgb<u8>) -> Lab {
    Lab::from_xyz(
        &SRgb::new().convert_to_xyz(&srgb.color_cast::<f64>().srgb_encoded()),
        WhitePoint::default(),
    )
}

fn lab2srgb(lab: &Lab) -> Rgb<u8> {
    SRgb::new().convert_from_xyz(&lab.to_xyz()).color_cast()
}

fn show_colors(colors: &[Lab]) {
    let colors: Vec<_> = colors.iter().map(lab2srgb).collect();
    print!("[");
    let mut str = String::new();
    for i in 0..colors.len() {
        str.clear();
        for j in 0..colors.len() {
            if i == j {
                continue;
            }
            let color = &colors[j];
            str += &format!(
                "{}",
                "▀".true_color(color.red(), color.green(), color.blue())
            );
        }
        let color = &colors[i];
        print!(
            "{}",
            (*str).on_true_color(color.red(), color.green(), color.blue())
        );
    }
    print!("]");
}

fn protanopia(color: &Lab) -> Lab {
    let mut lms = Lms::<f64, CieCam2002>::from_color(&color.to_xyz());
    lms.set_l(1.05118294 * lms.m() - 0.05116099 * lms.s());
    Lab::from_xyz(&Xyz::from_color(&lms), WhitePoint::default())
}

fn deuteranopia(color: &Lab) -> Lab {
    let mut lms = Lms::<f64, CieCam2002>::from_color(&color.to_xyz());
    lms.set_m(0.9513092 * lms.l() + 0.04866992 * lms.s());
    Lab::from_xyz(&Xyz::from_color(&lms), WhitePoint::default())
}

fn tritanopia(color: &Lab) -> Lab {
    let mut lms = Lms::<f64, CieCam2002>::from_color(&color.to_xyz());
    lms.set_s(1.86727089 * lms.m() - 0.86744736 * lms.l());
    Lab::from_xyz(&Xyz::from_color(&lms), WhitePoint::default())
}

fn print_colors(colors: &[Lab]) {
    let mut str = String::new();
    for cols in colors.chunks_exact(2) {
        let a = lab2srgb(&cols[0]);
        let b = lab2srgb(&cols[1]);
        str += &format!(
            "{}",
            "▀".true_color(a.red(), a.green(), a.blue()).on_true_color(
                b.red(),
                b.green(),
                b.blue(),
            )
        );
    }
    print!("|{}|", str);
}

fn test() {
    let mut colors = [Lab::default(); 16];
    let scale_rgb = |v| (v * 255 / 7) as u8;
    let from_rgb = |r, g, b| srgb2lab(&Rgb::new(scale_rgb(r), scale_rgb(g), scale_rgb(b)));
    let scale_lms = |v| (v as f64 / 7.0).powf(2.2);
    let from_lms = |l, m, s| {
        Lab::from_xyz(
            &Xyz::from_color(&Lms::<f64, CieCam2002>::new(
                scale_lms(l),
                scale_lms(m),
                scale_lms(s),
            )),
            WhitePoint::default(),
        )
    };
    let color_sources: [&dyn Fn(_, _, _) -> _; 2] = [&from_rgb, &from_lms];
    let color_conversions = [protanopia, deuteranopia, tritanopia];
    for b in 0..8 {
        for g in 0..4 {
            for color_source in &color_sources {
                for r in 0..8 {
                    colors[r * 2] = color_source(r, 2 * g, b);
                    colors[r * 2 + 1] = color_source(r, 2 * g + 1, b);
                }
                print_colors(&colors);
                for color_conv in &color_conversions {
                    print_colors(&colors.iter().map(color_conv).collect::<Vec<_>>());
                }
                print!("  ");
            }
            println!();
        }
    }
}

fn show_status(colors: &[Lab], cost: Option<f64>) {
    show_colors(colors);
    show_colors(&colors.iter().map(protanopia).collect::<Vec<_>>());
    show_colors(&colors.iter().map(deuteranopia).collect::<Vec<_>>());
    show_colors(&colors.iter().map(tritanopia).collect::<Vec<_>>());
    if let Some(cost) = cost {
        println!(" - Cost: {}", cost);
    } else {
        println!();
    }
}

fn calc_error(colors_org: &[Lab], colors_new: &[Lab]) -> f64 {
    assert_eq!(colors_org.len(), colors_new.len());
    let delta = |c1: &Lab, c2: &Lab| {
        (c2.L() - c1.L()).powi(2) + (c2.a() - c1.a()).powi(2) + (c2.b() - c1.b()).powi(2)
    };
    let mut it = colors_org.iter().zip(colors_new);
    let mut error = 0.0;
    while let Some((org, new)) = it.next() {
        error += delta(org, new);
        error += (delta(new, &Lab::new(new.L(), 0.0, 0.0)).sqrt()
            - delta(org, &Lab::new(org.L(), 0.0, 0.0)).sqrt())
        .powi(2);

        if new.L() < 0.0 || new.L() > 100.0 {
            error += ((new.L() - 50.0).abs() - 50.0).powi(2);
        }
        if new.a().abs() > 100.0 {
            error += (new.a().abs() - 100.0).powi(2);
        }
        if new.b().abs() > 100.0 {
            error += (new.b().abs() - 100.0).powi(2);
        }

        for (org2, new2) in it.clone() {
            let diff_original = delta(org, org2).sqrt();
            let diff_new = delta(new, new2).sqrt();
            let diff_new_protanopia = delta(&protanopia(new), &protanopia(new2)).sqrt();
            let diff_new_deuteranopia = delta(&deuteranopia(new), &deuteranopia(new2)).sqrt();
            let diff_new_tritanopia = delta(&tritanopia(new), &tritanopia(new2)).sqrt();
            error += (diff_new - diff_original).powi(2)
                + (diff_new_protanopia - diff_original).powi(2)
                + (diff_new_deuteranopia - diff_original).powi(2)
                + (diff_new_tritanopia - diff_original).powi(2);
        }
    }
    error
}

fn slice_to_lab(data: &[f64]) -> Vec<Lab> {
    assert_eq!(data.len() % 3, 0);
    data.chunks_exact(3)
        .map(|v| Lab::new(v[0], v[1], v[2]))
        .collect()
}

struct Colors(Vec<Lab>);

impl ArgminOp for Colors {
    type Param = Vec<f64>;
    type Output = f64;
    type Hessian = ();
    type Jacobian = ();
    type Float = f64;

    fn apply(&self, p: &Self::Param) -> Result<Self::Output, Error> {
        Ok(calc_error(&self.0, &slice_to_lab(p)))
    }
}

fn generate_params(colors: &[Lab]) -> Vec<Vec<f64>> {
    let mut args = Vec::new();
    args.reserve(colors.len() * 3);
    for color in colors {
        args.extend_from_slice(&[color.L(), color.a(), color.b()]);
    }

    let mut params = Vec::new();
    params.reserve(args.len() + 1);
    for i in 0..args.len() {
        let mut param = args.clone();
        let v = &mut param[i];
        *v = if v.abs() < 0.1 { 0.1 } else { *v * 1.1 };
        params.push(param);
    }
    params.push(args);
    params
}

struct MyObserver {}

impl Observe<Colors> for MyObserver {
    fn observe_iter(&mut self, state: &IterState<Colors>, _kv: &ArgminKV) -> Result<(), Error> {
        show_status(&slice_to_lab(&state.best_param), Some(state.cost));
        Ok(())
    }
}

fn main() -> Result<(), Error> {
    test();
    let colors = [
        (255, 0, 0),
        (255, 255, 0),
        (0, 255, 0),
        (0, 255, 255),
        (0, 0, 255),
        (255, 0, 255),
    ];
    let colors: Vec<_> = colors
        .iter()
        .map(|color| srgb2lab(&Rgb::from_tuple(*color)))
        .collect();
    show_status(&colors, None);

    let params = generate_params(&colors);

    let cost = Colors(colors);
    let solver = NelderMead::new().with_initial_params(params);
    let res = Executor::new(cost, solver, vec![])
        //.add_observer(MyObserver {}, ObserverMode::NewBest)
        .max_iters(10000)
        .run()?;

    show_status(&slice_to_lab(&res.state.best_param), Some(res.state.cost));
    Ok(())
}
